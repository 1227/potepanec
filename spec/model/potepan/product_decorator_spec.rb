RSpec.describe Potepan::ProductDecorator, type: :model do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:products) { create_list(:product, 4, taxons: [taxon]) }
  let!(:unrelated_products) { create(:product) }

  it '自身を除き、同カテゴリーの商品が表示されること' do
    expect(product.related_products).to contain_exactly(products[0],
                                                        products[1],
                                                        products[2],
                                                        products[3])
    expect(product.related_products).not_to include product
  end

  it '関連していない商品を表示しないこと' do
    expect(product.related_products).not_to include unrelated_products
  end
end
