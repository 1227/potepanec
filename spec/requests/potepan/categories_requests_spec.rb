RSpec.describe 'Categories_request', type: :request do
  describe 'GET /potepan/categories/:id' do
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:taxon) { create(:taxon) }

    before do
      get potepan_category_path(taxon.id)
    end

    it 'リクエストが成功すること' do
      expect(response).to have_http_status(200)
    end

    it 'showページが表示されること' do
      expect(response).to render_template :show
    end
  end
end
