RSpec.describe 'Products_request', type: :request do
  describe 'GET /potepan/products/:id' do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon) }
    let!(:products) { create_list(:product, 4, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it 'リクエストが成功すること' do
      expect(response).to have_http_status(200)
    end

    it 'showページが表示されること' do
      expect(response).to render_template :show
    end

    it '商品データが表示されること' do
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
      expect(response.body).to include product.description
    end

    it '関連商品が表示されること' do
      expect(response.body).to include products.first.name
      expect(response.body).to include products.first.display_price.to_s
      expect(Capybara.string(response.body)).to have_selector '.productBox', count: 4
    end
  end
end
