RSpec.describe 'Categories', type: :feature do
  describe 'GET /potepan/categories/:id' do
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:taxon) { create(:taxon, name: 'Bags', taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:taxonomy) { create(:taxonomy, name: 'Categories') }

    before do
      visit potepan_category_path(taxon.id)
    end

    it "商品カテゴリーをクリックすると分類と商品数が表示されること" do
      within("#category") do
        click_link taxonomy.name
        expect(page).to have_content taxon.name
        expect(page).to have_content taxon.products.count
      end
    end

    it '商品リンクから投稿詳細ページへ飛ぶこと' do
      within(".productBox") do
        click_link product.name
      end
      expect(current_path).to eq potepan_product_path(product.id)
    end

    it 'カテゴリに紐付いた商品データが表示されること' do
      within(".productBox") do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price.to_s
      end
    end

    it 'カテゴリに紐付かない商品データが表示されないこと' do
      within(".productBox") do
        expect(page).not_to have_content "test totn"
      end
    end
  end
end
