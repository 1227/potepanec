RSpec.describe 'Products', type: :feature do
  describe 'GET /potepan/products/:id' do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon) }
    let!(:products) { create_list(:product, 5, taxons: [taxon]) }
    let!(:unrelated_products) { create(:product) }

    before do
      visit potepan_product_path(product.id)
    end

    it 'HOMEリンクからトップページへ戻ること' do
      within(".breadcrumb") do
        click_link 'Home'
      end
      expect(current_path).to eq potepan_index_path
    end

    it '一覧ページへ戻るリンクからカテゴリー一覧へ戻ること' do
      within(".media-body") do
        click_link '一覧ページへ戻る'
      end
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    it '商品データが表示されること' do
      within(".media-body") do
        expect(page).to have_text product.name
        expect(page).to have_text product.display_price.to_s
        expect(page).to have_text product.description
      end
    end

    it '関連商品が表示されること' do
      within(".productsContent") do
        expect(page).to have_text products.first.name
        expect(page).to have_text products.first.display_price.to_s
        expect(page).to have_selector '.productBox', count: 4
      end
    end

    it '関連していない商品を表示しないこと' do
      within(".productsContent") do
        expect(page).not_to have_text unrelated_products.name
      end
    end

    it '関連商品から投稿詳細ページ飛ぶこと' do
      within(".productsContent") do
        click_link products.first.name
      end
      expect(current_path).to eq potepan_product_path(products.first.id)
    end
  end
end
